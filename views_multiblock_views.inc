<?php

/**
 * @file
 *	Provides a default view for views_multiblock.
 */

/**
 * Provides a default view for views_multiblock
 */
function views_multiblock_views_default_views() {
  $view = new stdClass();
  $view->name = 'views_multiblock';

  $view->description = 'Views_Multiblock Default View';
  $view->access = array ('anonymous user');
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = '';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'teaser';
  $view->url = 'vm';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->block = TRUE;
  $view->block_title = '';
  $view->block_header = '';
  $view->block_header_format = '1';
  $view->block_footer = '';
  $view->block_footer_format = '1';
  $view->block_empty = '';
  $view->block_empty_format = '1';
  $view->block_type = 'views_multiblock';
  $view->nodes_per_block = '1';
  $view->block_more = TRUE;
  $view->block_use_page_header = FALSE;
  $view->block_use_page_footer = FALSE;
  $view->block_use_page_empty = FALSE;
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'sticky',
      'sortorder' => 'ASC',
      'options' => '',
    ),
    array (
      'tablename' => 'node',
      'field' => 'changed',
      'sortorder' => 'DESC',
      'options' => 'normal',
    ),
    array (
      'tablename' => 'node',
      'field' => 'title',
      'sortorder' => 'ASC',
      'options' => '',
    ),
  );
  $view->argument = array (
    array (
      'type' => 'taxid',
      'argdefault' => '7',
      'title' => '%1',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
    array (
      'type' => 'node_feed',
      'argdefault' => '2',
      'title' => '',
      'options' => 'News Feed',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
    array (
      'tablename' => 'node',
      'field' => 'changed',
      'label' => '',
      'handler' => 'views_handler_field_date_small',
    ),
    array (
      'tablename' => 'node',
      'field' => 'body',
      'label' => '',
      'handler' => 'views_handler_field_teaser',
    ),

  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
  );
  
  $view->exposed_filter = array ();

  $view->requires = array(node);
  $views[$view->name] = $view;

  return $views;  
}

/**
 * Define additional view type names.
 */
function views_multiblock_info() {
  return array(
    'views_multiblock' => array('label' => 'Views_Multiblock'),
  );
}

/**
 *  Implementation of hook_views_style_plugins()
 */
function views_multiblock_views_style_plugins() {
  
  $plugins = array();
  $types = views_multiblock_view_types();
  foreach ($types as $name => $type) {
    $plugins[$name] = array(
      'name' => $type,
      'theme' => 'views_multiblock_display',
      'validate' => 'views_ui_plugin_validate_table',
      'needs_fields' => TRUE,
      'needs_table_header' => FALSE,
      );
  }
  
  return $plugins;
}

/**
 *  Possible views page display types
 */
function views_multiblock_view_types() {
  return array('views_multiblock' => t('Views_Multiblock'));
}

/**
 *  A form element for a view selector
 */
function _views_multiblock_view_type_form($view = NULL) {
  $form['views_multiblock_view'] = array(
    '#type' => 'select',
    '#options' => views_multiblock_view_types(),
    '#default_value' => $view ? $view->page_type : 'views_multiblock',
    '#title' => t('View'),
    );
  return $form;
}

/***********************************************************
 * THEME FUNCTIONS 
 */

/**
 *  Views_Multiblock plugin theme, overrides default views theme
 */
function theme_views_multiblock_display($view, $nodes) {
	//Organize the fields
	foreach($view->field as $field) {
		$fields[$field['fullname']] = $field;
	}
	
	//Generate output
	$output = NULL;
	foreach($nodes as $key => $node) {
		$output[] .= theme('views_multiblock_node', $node, $fields, $view);
	}

	$output = implode('<hr class="views_multiblock_separator" />' . "\n", $output);

	return $output;
}

/**
 * Theme an individual views_multiblock node
 */
function theme_views_multiblock_node($node, $fields, $view) {
	//Handles an apparent bug with views that modifies the $node->node_changed element to be $node->node_changed_changed instead.
	if(isset($node->node_changed_changed)) {
		$node->node_changed = $node->node_changed_changed;
		unset($node->node_changed_changed);
	}
	
	//Theme each field
	foreach($fields as $key => $field) {
		$fields[$key]['view'] = views_theme_field('views_handle_field', $field['queryname'], _views_get_fields(), $field, $node, $view);

	}

	//Build the output for each node
	$output = NULL;
	if(user_access('administer nodes')) {
		$output .= theme('views_multiblock_edit_link', $node);
	}
	foreach($fields as $key => $field) {
    if ($field['view']) {
      $output .= '<div class="views_multiblock_field views_multiblock_' . $field['field'] . '">' . $field['view'] . '</div>' . "\n";
    }
	}

	return $output;
	
}

/**
 * Theme a node edit link
 */
function theme_views_multiblock_edit_link($node) {
	return '<div style="float: right; position: relative; padding-top: 2em; font-size: 0.9em;"><sup>' . l(t('edit'), "node/$node->nid/edit") . '</sup></div>' . "\n";
}
