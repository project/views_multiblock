<?php

/**
 * @file
 *	Provides the ability to use one view in more than one block. 
 *
 *	Provides the ability to use one view in more than one block by
 * allowing users to specify views arguments in the block configuration
 * form.
 */

/***********************************************************
 * Drupal Hooks
 */
 
/**
 * Implementation of hook_menu().
 */
function views_multiblock_menu($may_cache) {
	$path = drupal_get_path('module', 'views_multiblock');
	require_once("./$path/views_multiblock_views.inc");
	
  drupal_add_css(drupal_get_path('module', 'views_multiblock') .'/views_multiblock.css');
  
  $items = array();

  if ($may_cache) {
    $items[] = array('path' => 'views_multiblock/increment',
      'access' => user_access('administer nodes'),
      'callback' => 'views_multiblock_story_number',
      'callback arguments' => array('direction' => 'increment'),
      'type' => MENU_CALLBACK,
    );
    $items[] = array('path' => 'views_multiblock/decrement',
      'access' => user_access('administer nodes'),
      'callback' => 'views_multiblock_story_number',
      'callback arguments' => array('direction' => 'decrement'),
      'type' => MENU_CALLBACK,
    );

		$items[] = array(
    	'path' => 'admin/settings/views_multiblock',
    	'title' => t('Views_Multiblock'),
    	'description' => t('These settings control the configurable global options for views_multiblock.'),
    	'callback' => 'drupal_get_form',
    	'callback arguments' => array('views_multiblock_admin_settings'),
    	'access' => user_access('administer site configuration'),
    	'type' => MENU_NORMAL_ITEM, // optional
		);
		
  }

  return $items;

}

/**
 * Implementation of hook_block().
 */
function views_multiblock_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
    	for($i = 0; $i < variable_get('views_multiblock_number', 5); $i++) {
      	$blocks[$i]['info'] = variable_get('views_multiblock_title_' . $i, t('Views Multiblock !d', array('!d' => $i))) . ' [vm]';
    	}
      return $blocks;
    case 'configure':
			return _views_multiblock_configure_form($delta);
    case 'save':
    	_views_multiblock_save_configuration($delta, $edit);
      break;
    case 'view':
    	//Figure out which view we are using, and load it.
    	$bypass = variable_get('views_multiblock_view_' . $delta, 0);

    	if($bypass != '0') {
    		$view_name = $bypass;
    	} else {
				$view_name = variable_get('views_multiblock_view', 'views_multiblock');
			}
			$view = views_get_view($view_name);
			
			//Use the view arguments to create a list of arguments to pass to the view
			$view_arguments = $view->argument;
			$view_argument_values = views_argument_api_get_processed('views_multiblock_' . $delta, NULL, $view_name);
			$arguments 						= views_argument_api_get_processed('views_multiblock_' . $delta, NULL, $view_name, TRUE);

			$GLOBALS['views_multiblock_current_arguments'] = $view_argument_values;

			$syndicate = FALSE;
			foreach($view_arguments as $argument) {
				if($argument['type']  == 'node_feed') {
					$syndicate = TRUE;
				}
			}

 			if (empty($view)) {
 				return;
 			}

 			$number_of_stories = variable_get('views_multiblock_items_' . $delta, 3);

			//Pass the block delta to the view, for anything that wants it.
			$view->block_delta = $delta;
			
			//Get the queries used to make the view
		  $path = drupal_get_path('module', 'views');
		  require_once("./$path/views_query.inc");
			$view_queries = _views_build_query($view, $arguments, array());
			
			if(empty($view_queries['countquery'])) {
				return;
			}
			
			$count = db_result(call_user_func_array('db_query', array_merge(array($view_queries['countquery']), $view_queries['args'])));

			if($count == 0) {
				return;
			}
						
 			//Use the block view so that we have control over the number of stories outputted without using the pager.
 			$view_output = views_build_view('block', $view, $arguments, FALSE, $number_of_stories);

 			if(empty($view_output)) {
 				return;
 			}
 			
 			//$view_output = theme_view($view_name, $number_of_stories, FALSE, 'block', $arguments);
			unset($GLOBALS['views_multiblock_current_arguments']);
			
			//Increment/Decrement Buttons
 			if(user_access('administer nodes')) {
 				$buttons = theme('views_multiblock_buttons', $delta, $count);
 			}

 			//Headers
 			$header = variable_get('views_multiblock_header_' . $delta, NULL);
 			if(!empty($header)) {
 				$header = theme('views_multiblock_header', $header);
 			}
 			
 			//Feed Icon
 			$feed_icon = NULL;
 			if($syndicate) {
 				$url = views_get_url($view, $arguments) . '/feed';
 				//$feed_icon = theme_feed_icon($url);
 				$feed_icons = drupal_add_feed($url, views_get_title($view));
 				$feed_icon = $feed_icons[$url];
 			}
 			
			$block['content'] = $buttons . $header . $view_output . $feed_icon;

			
			if($count > 0) {
      	return $block;
      } else {
      	return;
      }
    
  }
}

function views_multiblock_views_pre_view(&$view, $items) {
	$last_update = 0;
	foreach($items as $item) {
		if(isset($item->node_changed_changed) && $item->node_changed_changed > $last_update) {
			$last_update = $item->node_changed_changed;
		}
	}
	if($last_update > 0) {
		return theme('views_multiblock_last_update', $view, $last_update);
	}

}

/***********************************************************
 * Menu Callbacks
 */

/**
 * Admin settings callback().
 */
function views_multiblock_admin_settings() {
	//Default number of views_multiblock
	$form['views_multiblock_number'] = array('#title' => t('Number of blocks'),
		'#description' => t('Select the number of blocks to use on this site'),
		'#type' => 'select',
		'#options' => array(0, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40),
		'#default_value' => variable_get('views_multiblock_number', 5),
	);
	
	//Get all views
	$views = _views_multiblock_get_views();
	
	//Select a view
	$form['views_multiblock_view'] = array(
		'#title' => t('View'),
		'#type' => 'select',
		'#description' => t('Select which view to use with views_multiblock.'),
		'#options' => $views,
		'#default_value' => variable_get('views_multiblock_view', 'views_multiblock'),
	);
	
  return system_settings_form($form);
}

/**
 * Menu callback to change the nodes per block for a specific block.
 *
 * @param $direction
 *	Either 'increment' or 'decrement'.
 * @param $delta
 *	A block delta.
 */
function views_multiblock_story_number($direction, $delta) {
  $current_setting = variable_get('views_multiblock_items_' . arg(2), FALSE);
  if ($current_setting !== FALSE) {
    if ($direction == 'increment') {
      variable_set('views_multiblock_items_' . arg(2), ++$current_setting);
    }
    else {
      variable_set('views_multiblock_items_' . arg(2), --$current_setting);
    }
  }
  drupal_goto(referer_uri());
}


/***********************************************************
 * Theme Functions
 */

/**
 * Themes a set of buttons to change nodes_per_block.
 *
 * @param $delta
 *	A block delta.
 * @param $count
 *	The result of a views countquery for the specific block delta's view.
 * @return
 *	A themed set of buttons.
 */
function theme_views_multiblock_buttons($delta, $count) {
	$stories_per_block = variable_get('views_multiblock_items_' . $delta, 0);

 	$path = drupal_get_path('module', 'views_multiblock');
 	global $base_url;
 	
 	if($stories_per_block < $count) {
 		$plus = "$base_url/$path/plus.gif";
 		$plus = l('<img src="' . $plus . '" />', 'views_multiblock/increment/' . $delta, array(), NULL, NULL, FALSE, TRUE);
	}
	
	if($stories_per_block > 1) {
	 	$minus = "$base_url/$path/minus.gif";
 		$minus = l('<img src="' . $minus . '" />', 'views_multiblock/decrement/' . $delta, array(), NULL, NULL, FALSE, TRUE);
 	}
 	
	$buttons = '<div style="float: right; clear: both; position: relative; right: -10px; padding: 5px; border: 1px solid #ddd; background-color: #eee; margin: 5px;">' . $stories_per_block . '/' . $count . ' ' . $minus . $plus . '</div>';
	
	return $buttons;
}

/**
 * Themes a views_multiblock block header.
 *
 * @param $header
 *	The header text.
 * @return
 *	A themed block header.
 */
function theme_views_multiblock_header($header) {
	return '<div class="views_multiblock_block_header">' . $header . "</div>\n";
}

/**
 * Themes a last update time in a block.
 *
 * @param $view
 *	A view.
 * @param $last_update
 *	A timestamp that reflects the last update time.
 * @return
 *	A themed last update time.
 */
function theme_views_multiblock_last_update($view, $last_update) {
	$delta = $view->block_delta;
	$handler = variable_get('views_multiblock_update_time_' . $delta, 'none');
	if($handler != 'none') {
		return '<div class="views_multiblock_last_update">' . t('Last updated !d', array('!d' => format_date($last_update, $handler))) . "</div>\n";
	}
}

/***********************************************************
 * Helper Functions
 */

/**
 * Returns a block configuration form.
 *
 * The block configuration form includes any form elements that
 * have been created by modules that implement the
 * hook_views_multiblock_block hook.
 *
 * @param $delta
 *	A block delta.
 * @return
 *	A form object for use on the block configuration page.
 */
function _views_multiblock_configure_form($delta) {
	if(check_plain($_POST['op']) == t('Update')) {
		variable_set('views_multiblock_view_' . $delta, $form['bypass']['view_bypass']);
	}
	
	$form['#multistep'] = TRUE;
  $form['#redirect'] = FALSE;
  $form['#base'] = 'block_admin_configure';
  
	//Figure out which view we are using, and load it.
	$bypass = variable_get('views_multiblock_view_' . $delta, 0);
	
	if($bypass === 0) {
		$view_name = variable_get('views_multiblock_views', 'views_multiblock');
	} else {
		$view_name = $bypass;
	}
	$view = views_get_view($view_name);
	
	$date_handler_options = array(
		'none' => t('None'),
		'small' => t('Small'),
		'medium' => t('Medium'),
		'large' => t('Large'),
	);
	
	$form['title'] = array(
	  '#type' => 'textfield',
	  '#title' => 'Title',
	  '#default_value' => variable_get('views_multiblock_title_' . $delta, NULL),
	  '#weight' => -20
	);

	$form['items'] = array(
	  '#type' => 'select',
	  '#title' => t('Number of items to display'),
	  '#default_value' => variable_get('views_multiblock_items_' . $delta, 2) - 1,
	  '#options' => array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'),
	);
	$form['update_time'] = array(
		'#type' => 'select',
		'#title' => t('Update time'),
		'#description' => t('Select a method for showing the last-updated time of the block.'),
		'#default_value' => variable_get('views_multiblock_update_time_' . $delta, NULL),
		'#options' => $date_handler_options,
	);
	$form['header'] = array(
	  '#type' => 'textarea',
	  '#title' => 'Header',
	  '#default_value' => variable_get('views_multiblock_header_' . $delta, ''),
	  '#description' => t('Text for the header of the block.'),
	);
	$form['bypass'] = array(
		'#type' => 'fieldset',
		'#title' => t('View'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);
	$form['bypass']['view_bypass'] = array(
	  '#type' => 'select',
	  '#title' => 'View',
	  '#default_value' => variable_get('views_multiblock_view_' . $delta, -1),
	  '#description' => t('Select a view to use if you wish to bypass the default.'),
	  '#options' => array_merge(array('0' => t('--Default--')), _views_multiblock_get_views()),
	);
	$form['bypass']['redo_arg_form'] = array(
		'#type' => 'button',
		'#value' => t('Update'),
	);

	//Use the view arguments to create a list of configurable options for the block
	$view_arguments = $view->argument;
	$form['arguments'] = array(
		'#title' => t('View arguments'),
		'#type' => 'fieldset',
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);

	$views_argument_api_form = views_argument_api_build_argument_form($view_name);
	$views_argument_api_default_values = views_argument_api_get('views_multiblock_' . $delta, NULL);
	
	foreach($views_argument_api_form as $key => $element) {
		$form['arguments'][$key] = $element;
		$form['arguments'][$key]['#default_value'] = $views_argument_api_default_values[$key];
	}

  return $form;
}

/**
 * Saves the block configuration for a specific delta.
 *
 * The block configuration form includes any form elements that
 * have been created by modules that implement the
 * hook_views_multiblock_block hook. These get saved by an additional
 * call to that same hook.
 *
 * @param $delta
 *	A block delta.
 * @param $edit
 *	An array, passed to hook_block as the $edit argument.
 */
function _views_multiblock_save_configuration($delta, $edit = array()) {
	variable_set('views_multiblock_title_' . $delta, $edit['title']);
	variable_set('views_multiblock_items_' . $delta, $edit['items'] + 1);
	variable_set('views_multiblock_header_' . $delta, $edit['header']);
	variable_set('views_multiblock_view_' . $delta, $edit['view_bypass']);
	variable_set('views_multiblock_update_time_' . $delta, $edit['update_time']);
	
	
	//Figure out which view we are using, and load it.
	$bypass = variable_get('views_multiblock_view_' . $delta, 0);
	
	if($bypass === 0) {
		$view_name = variable_get('views_multiblock_views', 'views_multiblock');
	} else {
		$view_name = $bypass;
	}
	
	//Save module-specific argument settings
	views_argument_api_save_arguments('views_multiblock_' . $delta, $edit, $view_name);
}

/**
 * Returns a list of views, including default views.
 * @return
 *	An array of view names, keyed by view name.
 */
function _views_multiblock_get_views() {
	$views = array();
	$qs = db_query("SELECT v.vid, v.name FROM {view_view} v");
	while($obj = db_fetch_object($qs)) {
		$views[$obj->name] = $obj->name;
	}
  $default_views = module_invoke_all('views_default_views');
	foreach($default_views as $default_view) {
		$views[$default_view->name] = $default_view->name;
	}

	return $views;
}
